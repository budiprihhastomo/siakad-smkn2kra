$(document).ready(() => {
    get_datatable('data-kelas')
    get_datatable_multi('manage-siswa','siswa_belum_kelas')
    datatable('manage-kelas','siswa_kelas',id)
})

var id = $('#kelas').val()
var dd_kelas = $('#kelas')
    dd_kelas.select2(
        {
            theme: 'bootstrap',
            placeholder: 'Pilih Kelas',
            minimumInputLength: 3,
            ajax: {
                url: _BASE_URL + 'panel/dd/kelas',
                delay: 200,
                type: 'post',
                dataType: 'json',
                data: (filter) => {
                    return {
                        kd_kelas: filter.term
                    }
                },
                processResults: (response) => {
                    return {
                        results: $.map(response, (obj) => {
                            return {
                                id: obj.kd_kelas,
                                text: obj.kd_kelas
                            }
                        })
                    }
                },
            }
        }
    ).on('change',function(){
        datatable('manage-kelas','siswa_kelas',id).DataTable().ajax.reload()
    })

$('#btnTambah').click(() => {
    $('#master_modal').on('shown.bs.modal', () => {
        $('#form_master')[0].reset()
        $('#action').val('insert')
        $('#mapel, #kejuruan, #guru').select2({
            minimumInputLength: 3,
            placeholder: "Pilih Opsi",
            allowClear: true,
            theme: 'bootstrap',
            dropdownParent: $('#master_modal'),
        })
    }).modal('show')
})

if(_TABLE == 'guru')
{
    var tab = get_datatable('guru')
    tab.on('click', '.btn-edit', function(e) {
        var tr = $(this).closest("tr");
        var data = tab.DataTable().row(tr).data();
        $('#master_modal').on('shown.bs.modal', () => {
            $('#master_modal [name="action"]').val('update')
            $('#master_modal [name="nip"]').val(data[0])
            $('#master_modal [name="nama"]').val(data[1])
            $('#master_modal [name="mapel"]').val(getOptId(data[2])).change();
            $('#master_modal [name="kejuruan"]').val(getOptId(data[3])).change();
        }).modal('show')
    })
}
else if(_TABLE == 'siswa')
{
    var tab = get_datatable('siswa')
    tab.on('click', '.btn-edit', function(e) {
        var tr = $(this).closest("tr");
        var data = tab.DataTable().row(tr).data();
        $('#master_modal').on('shown.bs.modal', () => {
            $('#master_modal [name="action"]').val('update')
            $('#master_modal [name="nis"]').val(data[0])
            $('#master_modal [name="nama"]').val(data[1])
        }).modal('show')
    })
}
else if(_TABLE == 'kelas')
{
    var tab = get_datatable('kelas')
    tab.on('click', '.btn-edit', function(e) {
        var tr = $(this).closest("tr");
        var data = tab.DataTable().row(tr).data();
        $('#master_modal').on('shown.bs.modal', () => {
            $('#master_modal [name="action"]').val('update')
            $('#master_modal [name="kode"]').val(data[0])
            $('#master_modal [name="kejuruan"]').val(getOptId(data[1])).change();
            $('#master_modal [name="guru"]').val(getOptId(data[2])).change();
        }).modal('show')
    })
}
else if(_TABLE == 'mapel')
{
    var tab = get_datatable('mapel')
    tab.on('click', '.btn-edit', function(e) {
        var tr = $(this).closest("tr");
        var data = tab.DataTable().row(tr).data();
        $('#master_modal').on('shown.bs.modal', () => {
            $('#master_modal [name="action"]').val('update')
            $('#master_modal [name="kode"]').val(data[0])
            $('#master_modal [name="nama"]').val(data[1])
        }).modal('show')
    })
}
else if(_TABLE == 'kejuruan')
{
    var tab = get_datatable('kejuruan')
    tab.on('click', '.btn-edit', function(e) {
        var tr = $(this).closest("tr");
        var data = tab.DataTable().row(tr).data();
        $('#master_modal').on('shown.bs.modal', () => {
            $('#master_modal [name="action"]').val('update')
            $('#master_modal [name="kode"]').val(data[0])
            $('#master_modal [name="nama"]').val(data[1])
        }).modal('show')
    })
}
tab.on('click', '.btn-delete', function(e) {
    var tr = $(this).closest("tr");
    var data = tab.DataTable().row(tr).data();
    swal({title: "Apakah Anda Yakin?",icon: "warning",buttons: true,dangerMode: true,})
    .then((action) => {
        if(action)
        {
            $.post(_BASE_URL + 'panel/master/' + _TABLE + '/delete',{id:data[0]})
            .done((res)=>{
                if (res.result == 'success') {
                    swal({
                            title: "Berhasil!",
                            text: res.message,
                            icon: "success"
                        })
                        .then((act) => {
                            $('table').DataTable().ajax.reload()
                        });
                } else if (res.result == 'error') {
                    swal({
                        title: "Kesalahan!",
                        text: res.message,
                        icon: "error"
                    })
                }
            })
        }
    })
})

var myForm = $('#form_master')
myForm.submit(() => {
    var post = $.post(myForm.attr('action'), myForm.serialize())
        .done((res) => {
            if (res.result == 'success') {
                swal({
                        title: "Berhasil!",
                        text: res.message,
                        icon: "success"
                    })
                    .then((act) => {
                        $('#form_master')[0].reset()
                        $("select").val('').trigger('change.select2')
                        $('.modal').modal('toggle');
                        $('table').DataTable().ajax.reload()
                    });
            } else if (res.result == 'error') {
                swal({
                    title: "Kesalahan!",
                    text: res.message,
                    icon: "error"
                })
            }
        })
    return false;
})

function get_datatable(val) {
    var table = $('#dt-'+val)
    table.DataTable({
        bProcessing: true,
        sAjaxSource: _BASE_URL + 'panel/datatable/' + val,
        responsive: true
    });
    return table;
}

function datatable(table,val,id) {
    var table = $('#dt-'+table)
    table.dataTable({
        destroy: true,
        bProcessing: true,
        ajax: {
            url: _BASE_URL + 'panel/datatable/' + val,
            type: 'post',
            dataType: 'json',
            data: {id:id}
        }
    });
    return table;
}

function get_datatable_multi(table,val) {
    var table = $('#dt-'+table)
    table.DataTable({
        bProcessing: true,
        sAjaxSource: _BASE_URL + 'panel/datatable/' + val,
        responsive: true
    });
    return table;
}

function getOptId(text) {
    let id = '';
    $('select').find('*').filter(function() {
      if ($(this).text() === text) {
        id = $(this).val();
      }
    });
    return id;
  }