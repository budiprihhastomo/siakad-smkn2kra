<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Dashboard';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// Custom Route 
$route['panel/datatable/(:any)'] = 'BackEnd/DataTable/$1';
$route['panel/dashboard'] = 'Dashboard/index';
$route['panel/login'] = 'BackEnd/Login';
$route['panel/logout'] = 'BackEnd/Login/destroy';
$route['panel/login/auth'] = 'BackEnd/Login/auth';
$route['panel/master/guru'] = 'BackEnd/Master_Guru/index';
$route['panel/master/guru/save'] = 'BackEnd/Master_Guru/save';
$route['panel/master/guru/delete'] = 'BackEnd/Master_Guru/delete';
$route['panel/master/siswa'] = 'BackEnd/Master_Siswa/index';
$route['panel/master/siswa/save'] = 'BackEnd/Master_Siswa/save';
$route['panel/master/siswa/delete'] = 'BackEnd/Master_Siswa/delete';
$route['panel/master/kelas'] = 'BackEnd/Master_Kelas/index';
$route['panel/master/kelas/save'] = 'BackEnd/Master_Kelas/save';
$route['panel/master/kelas/delete'] = 'BackEnd/Master_Kelas/delete';
$route['panel/master/mapel'] = 'BackEnd/Master_Mapel/index';
$route['panel/master/mapel/save'] = 'BackEnd/Master_Mapel/save';
$route['panel/master/mapel/delete'] = 'BackEnd/Master_Mapel/delete';
$route['panel/master/kejuruan'] = 'BackEnd/Master_Kejuruan/index';
$route['panel/master/kejuruan/save'] = 'BackEnd/Master_Kejuruan/save';
$route['panel/master/kejuruan/delete'] = 'BackEnd/Master_Kejuruan/delete';
$route['panel/manage/kelas'] = 'BackEnd/Manage_Kelas/index';

$route['panel/dd/kelas'] = 'BackEnd/Manage_Kelas/get_list_kelas';