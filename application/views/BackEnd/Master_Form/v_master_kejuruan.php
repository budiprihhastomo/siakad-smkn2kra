<div class="page-inner">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6 pt-2">
                            <h4 class="page-title"><i class="fa fa-map mr-3"></i>Master Data Kejuruan</h4>
                        </div>
                        <div class="col-md-6 text-right">
                            <button id="btnTambah" class="btn btn-primary btn-md btn-round"><i class="fa fa-plus pr-2"></i> Tambah</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="dt-kejuruan" class="display table table-striped table-hover" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Kode Kejuruan</th>
                                    <th>Nama Kejuruan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Kode Kejuruan</th>
                                    <th>Nama Kejuruan</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="master_modal" role="dialog" aria-labelledby="master_modal" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
    <?=form_open('panel/master/kejuruan/save','id="form_master"')?>
    <input type="hidden" name="action" id="action">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Form Master Data Kejuruan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
					<label for="kode">Kode Kejuruan</label>
				    <input type="text" name="kode" class="form-control" id="kode" placeholder="Masukan Kode Kejuruan">
				</div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
					<label for="nama">Nama Kejuruan</label>
				    <input type="text" name="nama" class="form-control" id="text" placeholder="Masukan Nama Kejuruan">
				</div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
    <?=form_close()?>
    </div>
  </div>
</div>