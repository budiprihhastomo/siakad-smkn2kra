<div class="page-inner">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6 pt-2">
                            <h4 class="page-title"><i class="fa fa-school mr-3"></i>Master Data Kelas</h4>
                        </div>
                        <div class="col-md-6 text-right">
                            <button id="btnTambah" class="btn btn-primary btn-md btn-round"><i class="fa fa-plus pr-2"></i> Tambah</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="dt-kelas" class="display table table-striped table-hover" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Kode Kelas</th>
                                    <th>Kejuruan</th>
                                    <th>Guru</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Kode Kelas</th>
                                    <th>Kejuruan</th>
                                    <th>Guru</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="master_modal" role="dialog" aria-labelledby="master_modal" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
    <?=form_open('panel/master/kelas/save','id="form_master"')?>
    <input type="hidden" name="action" id="action">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Form Master Data Kelas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
					<label for="kode">Kode Kelas</label>
				    <input type="text" name="kode" class="form-control" id="kode" placeholder="Masukan Kode Kelas">
				</div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
					<label for="kejuruan">Kejuruan</label>
                    <div class="select2-input">
				        <select id="kejuruan" name="kejuruan" class="form-control">
                        <option value=""></option>
                        <?php foreach($dd_kejuruan as $row) { ?>
                        <option value="<?=$row->kd_jurusan?>"><?=$row->nama_jurusan?></option>
                        <?php } ?>
                        </select>
                    </div>
				</div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
					<label for="guru">Wali Kelas</label>
                    <div class="select2-input">
                        <select id="guru" name="guru" class="form-control">
                        <option value=""></option>
                        <?php foreach($dd_guru as $row) { ?>
                        <option value="<?=$row->nip?>"><?=$row->nama_guru?></option>
                        <?php } ?>
                        </select>
                    </div>
				</div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
    <?=form_close()?>
    </div>
  </div>
</div>