
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>SIAKAD SMKN2KRA - Sistem Akademik SMK Negeri 2 Karanganyar</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="<?=base_url('assets/img/icon.ico')?>" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="<?=base_url('assets/js/plugin/webfont/webfont.min.js')?>"></script>
	<link rel="stylesheet" href="<?=base_url('assets/css/404.css')?>">
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['<?=base_url('assets/css/fonts.min.css')?>']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="<?=base_url('assets/css/bootstrap.min.css')?>">
	<link rel="stylesheet" href="<?=base_url('assets/css/atlantis.min.css')?>">
</head>
<body>
	<div class="wrapper">
		<div class="main-header">
			<!-- Logo Header -->
			<div class="logo-header" data-background-color="blue">
				
				<a href="index.html" class="logo">
					<img src="<?=base_url('assets/img/logo.svg')?>" alt="navbar brand" class="navbar-brand">
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon">
						<i class="icon-menu"></i>
					</span>
				</button>
				<button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
				<div class="nav-toggle">
					<button class="btn btn-toggle toggle-sidebar">
						<i class="icon-menu"></i>
					</button>
				</div>
			</div>
			<!-- End Logo Header -->

			<!-- Navbar Header -->
			<nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">
				
				<div class="container-fluid">
					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
						<li class="nav-item toggle-nav-search hidden-caret">
							<a class="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
								<i class="fa fa-search"></i>
							</a>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
								<i class="fas fa-layer-group"></i>
							</a>
							<div class="dropdown-menu quick-actions quick-actions-info animated fadeIn">
								<div class="quick-actions-header">
									<span class="title mb-1">Quick Actions</span>
									<span class="subtitle op-8">Shortcuts</span>
								</div>
								<div class="quick-actions-scroll scrollbar-outer">
									<div class="quick-actions-items">
										<div class="row m-0">
											<a class="col-6 col-md-4 p-0" href="#">
												<div class="quick-actions-item">
													<i class="flaticon-file-1"></i>
													<span class="text">Generated Report</span>
												</div>
											</a>
											<a class="col-6 col-md-4 p-0" href="#">
												<div class="quick-actions-item">
													<i class="flaticon-database"></i>
													<span class="text">Create New Database</span>
												</div>
											</a>
											<a class="col-6 col-md-4 p-0" href="#">
												<div class="quick-actions-item">
													<i class="flaticon-pen"></i>
													<span class="text">Create New Post</span>
												</div>
											</a>
											<a class="col-6 col-md-4 p-0" href="#">
												<div class="quick-actions-item">
													<i class="flaticon-interface-1"></i>
													<span class="text">Create New Task</span>
												</div>
											</a>
											<a class="col-6 col-md-4 p-0" href="#">
												<div class="quick-actions-item">
													<i class="flaticon-list"></i>
													<span class="text">Completed Tasks</span>
												</div>
											</a>
											<a class="col-6 col-md-4 p-0" href="#">
												<div class="quick-actions-item">
													<i class="flaticon-file"></i>
													<span class="text">Create New Invoice</span>
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</nav>
			<!-- End Navbar -->
		</div>

		<!-- Sidebar -->
		<div class="sidebar sidebar-style-2">			
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
					<div class="user">
						<div class="avatar-sm float-left mr-2">
							<img src="<?=base_url('assets/img/profile.jpg')?>" alt="..." class="avatar-img rounded-circle">
						</div>
						<div class="info">
							<a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
								<span>
									<?=$this->session->userdata('user_full_name')?>
									<span class="user-level"><?=$this->session->userdata('user_level')?></span>
									<span class="caret"></span>
								</span>
							</a>
							<div class="clearfix"></div>

							<div class="collapse in" id="collapseExample">
								<ul class="nav">
									<li>
										<a href="<?=base_url('panel/logout')?>">
											<span class="link-collapse">Log Out</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<ul class="nav nav-primary">
						<li class="nav-item <?=($this->uri->segment(2) == 'dashboard' ? 'active' : '')?>">
							<a href="<?=base_url('panel/dashboard')?>"><i class="fas fa-home"></i><p>Dashboard</p></a>
						</li>
						<li class="nav-section">
							<span class="sidebar-mini-icon">
								<i class="fa fa-ellipsis-h"></i>
							</span>
							<h4 class="text-section">MENU UTAMA</h4>
						</li>
						<?php if($this->session->userdata('user_level') != 'Siswa'){?>
						<li class="nav-item <?=($this->uri->segment(2) == 'master' ? 'active submenu' : '')?>">
							<a data-toggle="collapse" href="#master-data" class="<?=($this->uri->segment(2) == 'master' ? '' : 'collapsed')?>" aria-expanded="<?=($this->uri->segment(2) == 'master' ? 'true' : '')?>">
								<i class="fas fa-database"></i>
								<p>Master Form</p>
								<span class="caret"></span>
							</a>
							<div class="collapse <?=($this->uri->segment(2) == 'master' ? 'show' : '')?>" id="master-data">
								<ul class="nav nav-collapse">
									<li class="<?=($this->uri->segment(3) == 'guru' ? 'active' : '')?>">
										<a href="<?=base_url('panel/master/guru')?>">
											<span class="sub-item">Master Guru</span>
										</a>
									</li>
									<li class="<?=($this->uri->segment(3) == 'siswa' ? 'active' : '')?>">
										<a href="<?=base_url('panel/master/siswa')?>">
											<span class="sub-item">Master Siswa</span>
										</a>
									</li>
									<li class="<?=($this->uri->segment(3) == 'kelas' ? 'active' : '')?>">
										<a href="<?=base_url('panel/master/kelas')?>">
											<span class="sub-item">Master Kelas</span>
										</a>
									</li>
									<li class="<?=($this->uri->segment(3) == 'mapel' ? 'active' : '')?>">
										<a href="<?=base_url('panel/master/mapel')?>">
											<span class="sub-item">Master Mapel</span>
										</a>
									</li>
									<li class="<?=($this->uri->segment(3) == 'kejuruan' ? 'active' : '')?>">
										<a href="<?=base_url('panel/master/kejuruan')?>">
											<span class="sub-item">Master Kejuruan</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<?php } if($this->session->userdata('user_level') != 'Siswa'){?>
						<li class="nav-item <?=($this->uri->segment(2) == 'manage' ? 'active submenu' : '')?>">
							<a data-toggle="collapse" href="#kelola-data" class="<?=($this->uri->segment(2) == 'manage' ? '' : 'collapsed')?>" aria-expanded="<?=($this->uri->segment(2) == 'manage' ? 'true' : '')?>">
								<i class="fas fa-edit"></i>
								<p>Pengelolaan Data</p>
								<span class="caret"></span>
							</a>
							<div class="collapse <?=($this->uri->segment(2) == 'manage' ? 'show' : '')?>" id="kelola-data">
								<ul class="nav nav-collapse">
									<li class="<?=($this->uri->segment(3) == 'nilai' ? 'active' : '')?>">
										<a href="<?=base_url('panel/manage/nilai')?>">
											<span class="sub-item">Kelola Nilai</span>
										</a>
									</li>
									<li class="<?=($this->uri->segment(3) == 'jadwal' ? 'active' : '')?>">
									<a href="<?=base_url('panel/manage/jadwal')?>">
											<span class="sub-item">Kelola Jadwal</span>
										</a>
									</li>
									<li class="<?=($this->uri->segment(3) == 'kelas' ? 'active' : '')?>">
										<a href="<?=base_url('panel/manage/kelas')?>">
											<span class="sub-item">Kelola Kelas</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
		<!-- End Sidebar -->

		<div class="main-panel">
			<div class="content">
				<?php if(isset($_content)) $this->load->view($_content)?>
			</div>
			<footer class="footer">
				<div class="container-fluid">
					<nav class="pull-left">
						<ul class="nav">
							<li class="nav-item">
								<a class="nav-link" href="http://www.themekita.com">
									ThemeKita
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">
									Help
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">
									Licenses
								</a>
							</li>
						</ul>
					</nav>
					<div class="copyright ml-auto">
						2018, made with <i class="fa fa-heart heart text-danger"></i> by <a href="http://www.themekita.com">ThemeKita</a>
					</div>				
				</div>
			</footer>
		</div>
	</div>
	<!--   Core JS Files   -->
	<script src="<?=base_url('assets/js/core/jquery.3.2.1.min.js')?>"></script>
	<script src="<?=base_url('assets/js/core/popper.min.js')?>"></script>
	<script src="<?=base_url('assets/js/core/bootstrap.min.js')?>"></script>

	<!-- jQuery UI -->
	<script src="<?=base_url('assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js')?>"></script>
	<script src="<?=base_url('assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js')?>"></script>
	<script src="<?=base_url('assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js')?>"></script>
	<script src="<?=base_url('assets/js/plugin/moment/moment.min.js')?>"></script>
	<script src="<?=base_url('assets/js/plugin/datatables/datatables.min.js')?>"></script>
	<script src="<?=base_url('assets/js/plugin/datepicker/bootstrap-datetimepicker.min.js')?>"></script>
	<script src="<?=base_url('assets/js/plugin/select2/select2.full.min.js')?>"></script>
	<script src="<?=base_url('assets/js/plugin/sweetalert/sweetalert.min.js')?>"></script>
	<!-- Atlantis JS -->
	<script src="<?=base_url('assets/js/atlantis.min.js')?>"></script>
	<script>
	var _BASE_URL = '<?=base_url()?>'
	var _TABLE = '<?=$this->uri->segment(3)?>'
	</script>
	<script src="<?=base_url('assets/js/action.js')?>"></script>
</html>