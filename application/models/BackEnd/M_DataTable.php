<?php defined('BASEPATH') OR exit('Dilarang Mengakses File ini !');

class M_DataTable extends CI_Model
{
    public static $GURU = 'siakad_guru';
    public static $SISWA = 'siakad_siswa';
    public static $KELAS = 'siakad_kelas';
    public static $MAPEL = 'siakad_mapel';
    public static $KEJURUAN = 'siakad_jurusan';

    public function dd_mapel()
    {
        return $this->db->get(self::$MAPEL)->result();
    }

    public function dd_guru()
    {
        return $this->db->get(self::$GURU)->result();
    }

    public function dd_kejuruan()
    {
        return $this->db->get(self::$KEJURUAN)->result();
    }

    public function master_guru()
    {
        $this->db->select('nip,nama_mapel,nama_jurusan,nama_guru');
        $this->db->join(self::$MAPEL.' T2','T1.kd_mapel=T2.kd_mapel','LEFT');
        $this->db->join(self::$KEJURUAN.' T3','T1.kd_jurusan=T3.kd_jurusan','LEFT');
        return $this->db->get(self::$GURU.' T1')->result();
    }

    public function master_siswa()
    {
        return $this->db->get(self::$SISWA.' T1')->result();
    }

    public function master_kelas()
    {
        $this->db->select('kd_kelas,nama_jurusan,nama_guru');
        $this->db->join(self::$KEJURUAN.' T2','T1.kd_jurusan=T2.kd_jurusan','LEFT');
        $this->db->join(self::$GURU.' T3','T1.nip=T3.nip','LEFT');
        return $this->db->get(self::$KELAS.' T1')->result();
    }

    public function master_mapel()
    {
        return $this->db->get(self::$MAPEL.' T1')->result();
    }

    public function master_kejuruan()
    {
        return $this->db->get(self::$KEJURUAN.' T1')->result();
    }
}
