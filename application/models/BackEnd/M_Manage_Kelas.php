<?php defined('BASEPATH') OR exit('Dilarang Mengakses File ini !');

class M_Manage_Kelas extends CI_Model
{
    public static $PK = 'id';
    public static $RELASI = 'siakad_rel_kelas';
    public static $KELAS = 'siakad_kelas';
    public static $SISWA = 'siakad_siswa';

    public function get_siswa_id()
    {
        return $this->db->get(self::$RELASI)->result();
    }

    public function get_kelas($filter)
    {
        $this->db->like('kd_kelas',$filter,'both');
        return $this->db->get(self::$KELAS)->result();
    }

    public function get_siswa_in_class($kelas)
    {
        $this->db->select('T1.nis,T2.nama_siswa');
        $this->db->join(self::$SISWA.' T2','T1.nis=T2.nis','LEFT');
        $this->db->join(self::$KELAS.' T3','T1.kd_kelas=T3.kd_kelas','LEFT');
        $this->db->where('T1.kd_kelas', $kelas);
        return $this->db->get(self::$RELASI.' T1')->result();
    }

    public function get_siswa_not_in_class($filter=[])
    {
        $this->db->select('T1.nis,T1.nama_siswa');
        $this->db->join(self::$RELASI.' T2','T1.nis=T2.nis','LEFT');
        $this->db->where_not_in('T1.nis', $filter);
        return $this->db->get(self::$SISWA.' T1')->result();
    }
}
