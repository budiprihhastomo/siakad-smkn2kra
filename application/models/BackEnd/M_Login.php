<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Login extends CI_Model {
    public static $PK = 'id';
    public static $TB = 'siakad_pengguna';
    public static $SI = 'siakad_siswa';
    public static $GU = 'siakad_guru';

    public function get_user($user_id, $user_password)
    {
        $this->db->select('X1.id_user,X1.user_level,X2.nama_siswa as siswa,X3.nama_guru as guru');
        $this->db->join(self::$SI.' X2','X1.id_user=X2.nis','LEFT');
        $this->db->join(self::$GU.' X3','X1.id_user=X3.nip','LEFT');
        $this->db->where('id_user', $user_id);
        $this->db->where('user_password', $user_password);
        return $this->db->get(self::$TB.' X1');
    }
}
