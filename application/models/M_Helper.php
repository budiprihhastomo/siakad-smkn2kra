<?php defined('BASEPATH') OR exit('Dilarang Mengakses File ini !');

class M_Helper extends CI_Model
{
    public function ActionInsert($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->affected_rows();
    }
    
    public function ActionUpdate($table, $data, $filter)
    {
        $this->db->update($table, $data, $filter);
        return $this->db->affected_rows();
    }
    
    public function ActionDelete($table, $filter)
    {
        $this->db->delete($table, $filter);
        return $this->db->affected_rows();
    }

    public function FetchRow($table, $filter)
    {
        return $this->db->get_where($table, $filter);
    }

    public function FetchData($table)
    {
        return $this->db->get($table)->result();
    }
}
