<?php defined('BASEPATH') OR exit('Dilarang Mengakses File ini !');

class Dashboard extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->is_auth();
    }

    public function index()
    {
        $this->var['_content'] = 'BackEnd/v_dashboard';
        $this->renderView($this->var);
    }
}
