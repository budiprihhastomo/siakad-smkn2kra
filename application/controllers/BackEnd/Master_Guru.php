<?php defined('BASEPATH') OR exit('Dilarang Mengakses File ini !');

class Master_Guru extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->is_auth();
    }

    public function index()
    {
        $this->var['_content'] = 'BackEnd/Master_Form/v_master_guru';
        $this->var['dd_mapel'] = $this->Datatable->dd_mapel();
        $this->var['dd_kejuruan'] = $this->Datatable->dd_kejuruan();
        $this->renderView($this->var);
    }

    public function save()
    {
        $act = $this->input->post('action');
        if($this->input->is_ajax_request())
        {
            if($this->is_valid($act))
            {
                $data = $this->data_save();
                if($act == 'insert')
                {
                    $this->var['result'] = ($this->Helper->ActionInsert('siakad_guru', $data) > 0 ? 'success' : 'error');
                    $this->var['message'] = ($this->var['result'] == 'success' ? 'Data Berhasil Disimpan' : 'Gagal Menyimpan Data');
                }
                else if($act == 'update')
                {
                    $nip = $this->input->post('nip');
                    $this->var['result'] = ($this->Helper->ActionUpdate('siakad_guru', $data, ['nip'=>$nip]) > 0 ? 'success' : 'error');
                    $this->var['message'] = ($this->var['result'] == 'success' ? 'Data Berhasil Diperbarui' : 'Gagal Memperbarui Data');
                }
            }
            else
            {
                $this->var['result'] = 'error';
                $this->var['message'] = validation_errors();
            }
            $this->output
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($this->var, JSON_PRETTY_PRINT))
                ->_display();
            exit;
        }
    }

    public function delete()
    {
        if($this->input->is_ajax_request())
        {
            $nip = $this->input->post('id');
            $this->var['result'] = ($this->Helper->ActionDelete('siakad_guru', ['nip'=>$nip]) > 0 ? 'success' : 'error');
            $this->var['message'] = ($this->var['result'] == 'success' ? 'Data Berhasil Dihapus' : 'Gagal Menghapus Data');
            $this->output
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($this->var, JSON_PRETTY_PRINT))
                ->_display();
            exit;
        }
    }

    protected function data_save()
    {
        return 
        [
            'nip' => $this->input->post('nip'),
            'kd_mapel' => $this->input->post('mapel'),
            'kd_jurusan' => $this->input->post('kejuruan'),
            'nama_guru' => $this->input->post('nama')
        ];
    }

    protected function is_valid($act)
    {
        if($act == 'insert')
        {
            $this->form_validation->set_rules('nip','NIP','trim|required|min_length[18]|max_length[18]|is_unique[siakad_guru.nip]');   
        }
        $this->form_validation->set_rules('mapel','Mata Pelajaran','required');
        $this->form_validation->set_rules('kejuruan','Kejuruan','required');
        $this->form_validation->set_rules('nama','Nama Guru','required');
        $this->form_validation->set_error_delimiters('', '');
        return $this->form_validation->run();
    }
}
