<?php defined('BASEPATH') OR exit('Dilarang Mengakses File ini !');

class Manage_Kelas extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('BackEnd/M_Manage_Kelas','Kelas');
        $this->is_auth();
    }

    public function index()
    {
        $this->var['_content'] = 'BackEnd/Management_Form/v_manage_kelas';
        $this->renderView($this->var);
    }

    public function save()
    {
        $act = $this->input->post('action');
        if($this->input->is_ajax_request())
        {
            if($this->is_valid($act))
            {
                $data = $this->data_save();
                if($act == 'insert')
                {
                    $this->var['result'] = ($this->Helper->ActionInsert('siakad_mapel', $data) > 0 ? 'success' : 'error');
                    $this->var['message'] = ($this->var['result'] == 'success' ? 'Data Berhasil Disimpan' : 'Gagal Menyimpan Data');
                }
                else if($act == 'update')
                {
                    $kd_mapel = $this->input->post('kode');
                    $this->var['result'] = ($this->Helper->ActionUpdate('siakad_mapel', $data, ['kd_mapel'=>$kd_mapel]) > 0 ? 'success' : 'error');
                    $this->var['message'] = ($this->var['result'] == 'success' ? 'Data Berhasil Diperbarui' : 'Gagal Memperbarui Data');
                }
            }
            else
            {
                $this->var['result'] = 'error';
                $this->var['message'] = validation_errors();
            }
            $this->output
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($this->var, JSON_PRETTY_PRINT))
                ->_display();
            exit;
        }
    }

    public function delete()
    {
        if($this->input->is_ajax_request())
        {
            $kd_mapel = $this->input->post('id');
            $this->var['result'] = ($this->Helper->ActionDelete('siakad_mapel', ['kd_mapel'=>$kd_mapel]) > 0 ? 'success' : 'error');
            $this->var['message'] = ($this->var['result'] == 'success' ? 'Data Berhasil Dihapus' : 'Gagal Menghapus Data');
            $this->output
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($this->var, JSON_PRETTY_PRINT))
                ->_display();
            exit;
        }
    }

    public function get_list_kelas()
    {
        if($this->input->is_ajax_request())
        {
            $filter = $this->input->post('kd_kelas');
            $this->var = $this->Kelas->get_kelas($filter);
            $this->output
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($this->var, JSON_PRETTY_PRINT))
                ->_display();
            exit;
        }
    }

    protected function data_save()
    {
        return 
        [
            'kd_mapel' => $this->input->post('kode'),
            'nama_mapel' => $this->input->post('nama'),
        ];
    }

    protected function is_valid($act)
    {
        if($act == 'insert')
        {
            $this->form_validation->set_rules('kode','Kode Kelas','trim|required|is_unique[siakad_mapel.kd_mapel]');   
        }
        $this->form_validation->set_rules('nama','Nama Mata Pelajaran','required');
        $this->form_validation->set_error_delimiters('', '');
        return $this->form_validation->run();
    }
}
