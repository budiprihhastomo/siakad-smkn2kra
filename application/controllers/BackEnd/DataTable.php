<?php defined('BASEPATH') OR exit('Dilarang Mengakses File ini !');

class DataTable extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('BackEnd/M_Manage_Kelas','MK');
        $this->is_auth();
    }

    public function guru()
    {
        $rows = $this->Datatable->master_guru();
        $data = [];
        foreach($rows as $row)
        {
            $data[] = array(
                $row->nip,
                $row->nama_guru,
                $row->nama_mapel,
                $row->nama_jurusan,
				'<button class="btn btn-sm btn-warning btn-round btn-icon btn-edit">
                <i class="fa fa-user-edit"></i>
                </button>
                <button class="btn btn-sm btn-danger btn-round btn-icon btn-delete">
                <i class="fa fa-trash"></i>
                </button>'
            );
        }
        $this->vars = ['data' => $data];
        $this->output
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->vars, JSON_PRETTY_PRINT))
            ->_display();
        exit;
    }

    public function siswa()
    {
        $rows = $this->Datatable->master_siswa();
        $data = [];
        foreach($rows as $row)
        {
            $data[] = array(
                $row->nis,
                $row->nama_siswa,
				'<button class="btn btn-sm btn-warning btn-round btn-icon btn-edit">
                <i class="fa fa-user-edit"></i>
                </button>
                <button class="btn btn-sm btn-danger btn-round btn-icon btn-delete">
                <i class="fa fa-trash"></i>
                </button>'
            );
        }
        $this->vars = ['data' => $data];
        $this->output
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->vars, JSON_PRETTY_PRINT))
            ->_display();
        exit;
    }

    public function kelas()
    {
        $rows = $this->Datatable->master_kelas();
        $data = [];
        foreach($rows as $row)
        {
            $data[] = array(
                $row->kd_kelas,
                $row->nama_jurusan,
                $row->nama_guru,
				'<button class="btn btn-sm btn-warning btn-round btn-icon btn-edit">
                <i class="fa fa-user-edit"></i>
                </button>
                <button class="btn btn-sm btn-danger btn-round btn-icon btn-delete">
                <i class="fa fa-trash"></i>
                </button>'
            );
        }
        $this->vars = ['data' => $data];
        $this->output
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->vars, JSON_PRETTY_PRINT))
            ->_display();
        exit;
    }

    public function mapel()
    {
        $rows = $this->Datatable->master_mapel();
        $data = [];
        foreach($rows as $row)
        {
            $data[] = array(
                $row->kd_mapel,
                $row->nama_mapel,
				'<button class="btn btn-sm btn-warning btn-round btn-icon btn-edit">
                <i class="fa fa-user-edit"></i>
                </button>
                <button class="btn btn-sm btn-danger btn-round btn-icon btn-delete">
                <i class="fa fa-trash"></i>
                </button>'
            );
        }
        $this->vars = ['data' => $data];
        $this->output
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->vars, JSON_PRETTY_PRINT))
            ->_display();
        exit;
    }

    public function kejuruan()
    {
        $rows = $this->Datatable->master_kejuruan();
        $data = [];
        foreach($rows as $row)
        {
            $data[] = array(
                $row->kd_jurusan,
                $row->nama_jurusan,
				'<button class="btn btn-sm btn-warning btn-round btn-icon btn-edit">
                <i class="fa fa-user-edit"></i>
                </button>
                <button class="btn btn-sm btn-danger btn-round btn-icon btn-delete">
                <i class="fa fa-trash"></i>
                </button>'
            );
        }
        $this->vars = ['data' => $data];
        $this->output
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->vars, JSON_PRETTY_PRINT))
            ->_display();
        exit;
    }

    public function siswa_belum_kelas()
    {
        foreach($this->MK->get_siswa_id() as $row)
        {
            $id_siswa[] = $row->nis;
        }
        $rows = $this->MK->get_siswa_not_in_class($id_siswa);
        $data = [];
        foreach($rows as $row)
        {
            $data[] = array(
                '<input type="checkbox"></input>',
                $row->nis,
                $row->nama_siswa
            );
        }
        $this->vars = ['data' => $data];
        $this->output
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->vars, JSON_PRETTY_PRINT))
            ->_display();
        exit;
    }

    public function siswa_kelas()
    {
        $id_siswa = $this->input->post('id');
        $rows = $this->MK->get_siswa_in_class($id_siswa);
        $data = [];
        foreach($rows as $row)
        {
            $data[] = array(
                '<input type="checkbox"></input>',
                $row->nis,
                $row->nama_siswa,
                '<button class="btn btn-sm btn-warning btn-round btn-icon btn-edit">
                <i class="fa fa-user-edit"></i>
                </button>
                <button class="btn btn-sm btn-danger btn-round btn-icon btn-delete">
                <i class="fa fa-trash"></i>
                </button>'
            );
        }
        $this->vars = ['data' => $data];
        $this->output
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->vars, JSON_PRETTY_PRINT))
            ->_display();
        exit;
    }
}
