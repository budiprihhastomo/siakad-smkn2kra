<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {
	public function __construct()
	{
        parent::__construct();
        $this->load->model('BackEnd/M_Login','AUTH');
	}
	
	public function index()
	{
        $this->load->view('BackEnd/v_login');
        if($this->session->userdata('is_logged') == TRUE)
        {
            redirect('panel/dashboard','refresh');
        }
    }
    
    public function auth()
    {
        if($this->input->is_ajax_request())
        {
            $user_email = $this->input->post('id_user');
            $user_password = $this->input->post('password');
            if($this->is_valid())
            {
                $user_check = $this->AUTH->get_user($user_email, $user_password);
                if($user_check->num_rows() > 0)
                {
                    $user_data = $user_check->row();
                    $session_data['id_user'] = $user_data->id_user;
                    $session_data['user_level'] = $user_data->user_level;
                    $session_data['is_logged'] = TRUE;
                    if($session_data['user_level'] == 'Siswa')
                    {
                        $session_data['user_full_name'] = $user_data->siswa;
                    }
                    else if($session_data['user_level'] == 'Guru')
                    {
                        $session_data['user_full_name'] = $user_data->guru;
                    }
                    else
                    {
                        $session_data['user_full_name'] = 'Administrator';
                    }
                    $this->session->set_userdata($session_data);
                    $this->vars =
                    [
                        'result' => 'login_success',
                        'msg' => show_message('login_success'),
                        'redirect_url' => base_url('panel/dashboard'),
                        'redirect' => 'true'
                    ];
                }
                else
                {
                    $this->vars = 
                    [
                        'result' => 'login_failed',
                        'msg' => show_message('login_failed'),
                        'redirect' => 'false'
                    ];
                }
            }
            else
            {
                $this->vars = 
				[
					'result' => 'error',
					'msg' => validation_errors()
				];
            }
            $this->output
            	->set_content_type('application/json', 'utf-8')
            	->set_output(json_encode($this->vars, JSON_PRETTY_PRINT))
            	->_display();
            exit;
        }
        else
		{
			$this->redirect_404();
			$this->render_view($this->vars);
		}
    }

    public function destroy()
    {
        $this->session->sess_destroy();
        $this->session->set_userdata(['is_logged' => FALSE]);
        redirect(base_url('panel/login'),'refresh'); 
    }

    public function is_valid()
    {
        $this->form_validation->set_rules('id_user','Username','trim|required', ['required'=>'%s masih kosong.']);
        $this->form_validation->set_rules('password','Password','trim|required', ['required'=>'%s masih kosong.']);
        $this->form_validation->set_error_delimiters('','');
        return $this->form_validation->run();
    }
}