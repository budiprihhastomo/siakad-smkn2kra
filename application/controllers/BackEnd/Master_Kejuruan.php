<?php defined('BASEPATH') OR exit('Dilarang Mengakses File ini !');

class Master_Kejuruan extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->is_auth();
    }

    public function index()
    {
        $this->var['_content'] = 'BackEnd/Master_Form/v_master_kejuruan';
        $this->renderView($this->var);
    }

    public function save()
    {
        $act = $this->input->post('action');
        if($this->input->is_ajax_request())
        {
            if($this->is_valid($act))
            {
                $data = $this->data_save();
                if($act == 'insert')
                {
                    $this->var['result'] = ($this->Helper->ActionInsert('siakad_jurusan', $data) > 0 ? 'success' : 'error');
                    $this->var['message'] = ($this->var['result'] == 'success' ? 'Data Berhasil Disimpan' : 'Gagal Menyimpan Data');
                }
                else if($act == 'update')
                {
                    $kd_jurusan = $this->input->post('kode');
                    $this->var['result'] = ($this->Helper->ActionUpdate('siakad_jurusan', $data, ['kd_jurusan'=>$kd_jurusan]) > 0 ? 'success' : 'error');
                    $this->var['message'] = ($this->var['result'] == 'success' ? 'Data Berhasil Diperbarui' : 'Gagal Memperbarui Data');
                }
            }
            else
            {
                $this->var['result'] = 'error';
                $this->var['message'] = validation_errors();
            }
            $this->output
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($this->var, JSON_PRETTY_PRINT))
                ->_display();
            exit;
        }
    }

    public function delete()
    {
        if($this->input->is_ajax_request())
        {
            $kd_jurusan = $this->input->post('id');
            $this->var['result'] = ($this->Helper->ActionDelete('siakad_jurusan', ['kd_jurusan'=>$kd_jurusan]) > 0 ? 'success' : 'error');
            $this->var['message'] = ($this->var['result'] == 'success' ? 'Data Berhasil Dihapus' : 'Gagal Menghapus Data');
            $this->output
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($this->var, JSON_PRETTY_PRINT))
                ->_display();
            exit;
        }
    }

    protected function data_save()
    {
        return 
        [
            'kd_jurusan' => $this->input->post('kode'),
            'nama_jurusan' => $this->input->post('nama'),
        ];
    }

    protected function is_valid($act)
    {
        if($act == 'insert')
        {
            $this->form_validation->set_rules('kode','Kode Kejuruan','trim|required|is_unique[siakad_jurusan.kd_jurusan]');   
        }
        $this->form_validation->set_rules('nama','Nama Kejuruan','required');
        $this->form_validation->set_error_delimiters('', '');
        return $this->form_validation->run();
    }
}
