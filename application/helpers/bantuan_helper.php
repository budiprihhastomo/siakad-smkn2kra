<?php
if(!function_exists('error_message'))
{
    function show_message($input,$action = false)
    {
        switch($action)
        {
            case 1:
            $act = 'menyimpan';
            break;
            
            case 2:
            $act = 'mengubah';
            break;

            case 3:
            $act = 'menghapus';
            break;
        }

        switch($input)
        {
            case 'success':
            $msg = 'Berhasil '.$act.' data dari database.';
            break;

            case 'warning':
            $msg = 'Tidak dapat '.$act.' data karena sedang digunakan.';
            break;

            case 'error':
            $msg = 'Gagal '.$act.' data dari database.';
            break;

            case 'info':
            $msg = 'Tidak ada perubahan data pada database.';
            break;

            case 'exist':
            $msg = 'NIK sudah terdaftar pada database, periksa kembali.';
            break;

            case 'login_success':
            $msg = 'Tunggu halaman akan segera dialihkan.';
            break;

            case 'login_failed':
            $msg = 'Akses ditolak.';
            break;

            default:
                $msg = 'Kesalahan pada sistem, Hubungi administrator.';
        }
        return $msg;
    }
}
?>