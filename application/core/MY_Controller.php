<?php defined('BASEPATH') OR exit('Dilarang Mengakses File ini !');

class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function renderView($data=[])
    {
        $this->load->view('BackEnd/structured_view',$data);
    }

    public function show_404_custom()
    {
        return $this->var['_content'] = 'BackEnd/error';
    }

    public function is_auth()
    {
        if($this->session->userdata('user_level') == FALSE)
        {
            redirect(base_url('panel/login'),'refresh');
        }
    }
}
