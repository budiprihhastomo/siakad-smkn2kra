-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 14, 2019 at 09:40 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_siakad_smkn2kra`
--

-- --------------------------------------------------------

--
-- Table structure for table `siakad_guru`
--

CREATE TABLE `siakad_guru` (
  `nip` varchar(18) NOT NULL,
  `kd_mapel` varchar(5) NOT NULL,
  `kd_jurusan` varchar(5) NOT NULL,
  `nama_guru` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `siakad_guru`
--

INSERT INTO `siakad_guru` (`nip`, `kd_mapel`, `kd_jurusan`, `nama_guru`) VALUES
('198503302003121002', 'RPL-1', 'RPL', 'Bambang Supeno'),
('198503302003121003', 'RPL-2', 'RPL', 'Wijayanti Eka Nanditya'),
('198503302003121004', 'RPL-3', 'RPL', 'Susi Winingsih'),
('198503302003121005', 'C.413', 'UMU', 'Wijayanti Kusuma');

-- --------------------------------------------------------

--
-- Table structure for table `siakad_jadwal`
--

CREATE TABLE `siakad_jadwal` (
  `id` int(11) NOT NULL,
  `nip` varchar(18) NOT NULL,
  `kd_kelas` int(11) NOT NULL,
  `kd_mapel` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `siakad_jurusan`
--

CREATE TABLE `siakad_jurusan` (
  `kd_jurusan` varchar(5) NOT NULL,
  `nama_jurusan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `siakad_jurusan`
--

INSERT INTO `siakad_jurusan` (`kd_jurusan`, `nama_jurusan`) VALUES
('AKU', 'Akutansi'),
('HOT', 'Perhotelan'),
('MES', 'Teknik Pemesinan'),
('OTO', 'Teknik Otomotif Elektronika'),
('RPL', 'Rekayasa Perangkat Lunak'),
('TEK', 'Teknik Perkainan'),
('TKI', 'Teknik Komputer dan Informasi'),
('UMU', 'Umum');

-- --------------------------------------------------------

--
-- Table structure for table `siakad_kelas`
--

CREATE TABLE `siakad_kelas` (
  `kd_kelas` varchar(5) NOT NULL,
  `kd_jurusan` varchar(5) NOT NULL,
  `nip` varchar(18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `siakad_kelas`
--

INSERT INTO `siakad_kelas` (`kd_kelas`, `kd_jurusan`, `nip`) VALUES
('XIRA', 'RPL', '198503302003121005'),
('XIRB', 'RPL', '198503302003121003'),
('XIRC', 'RPL', '198503302003121002');

-- --------------------------------------------------------

--
-- Table structure for table `siakad_mapel`
--

CREATE TABLE `siakad_mapel` (
  `kd_mapel` varchar(5) NOT NULL,
  `nama_mapel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `siakad_mapel`
--

INSERT INTO `siakad_mapel` (`kd_mapel`, `nama_mapel`) VALUES
('314', 'Matematika'),
('315', 'Bahasa Indonesia'),
('316', 'Bahasa Jawa'),
('C.412', 'Kimia'),
('C.413', 'Fisika'),
('C.414', 'Biologi'),
('RPL-1', 'Pemrograman Dasar'),
('RPL-2', 'Pemrograman Berbasis Objek'),
('RPL-3', 'Pemrograman Web Dinamis');

-- --------------------------------------------------------

--
-- Table structure for table `siakad_nilai`
--

CREATE TABLE `siakad_nilai` (
  `nis` int(11) NOT NULL,
  `uts` int(11) NOT NULL,
  `uas` int(11) NOT NULL,
  `kd_kelas` int(11) NOT NULL,
  `kd_mapel` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `siakad_pengguna`
--

CREATE TABLE `siakad_pengguna` (
  `id_user` varchar(18) NOT NULL,
  `user_password` varchar(32) NOT NULL,
  `user_level` enum('Administrator','Guru','Siswa') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `siakad_pengguna`
--

INSERT INTO `siakad_pengguna` (`id_user`, `user_password`, `user_level`) VALUES
('198503302003121002', 'guru', 'Guru'),
('6078', 'siswa', 'Siswa'),
('admin', 'admin', 'Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `siakad_rel_kelas`
--

CREATE TABLE `siakad_rel_kelas` (
  `nis` int(11) NOT NULL,
  `kd_kelas` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `siakad_rel_kelas`
--

INSERT INTO `siakad_rel_kelas` (`nis`, `kd_kelas`) VALUES
(6078, 'XIIRC'),
(6081, 'XIIRC');

-- --------------------------------------------------------

--
-- Table structure for table `siakad_siswa`
--

CREATE TABLE `siakad_siswa` (
  `nis` int(11) NOT NULL,
  `nama_siswa` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `siakad_siswa`
--

INSERT INTO `siakad_siswa` (`nis`, `nama_siswa`) VALUES
(6078, 'Budi Prih Hastomo'),
(6079, 'Dwi Purnomo'),
(6080, 'Erlangga Eka Nanditya'),
(6081, 'Esti Setyaningrum'),
(6082, 'Fahrul Nur Seha'),
(6083, 'Firqi Firnansyah'),
(6084, 'Herlambang Teguh Imanto'),
(6085, 'Lintang Agustina'),
(6086, 'Lucky Febriyanto');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `siakad_guru`
--
ALTER TABLE `siakad_guru`
  ADD PRIMARY KEY (`nip`),
  ADD KEY `kd_mapel` (`kd_mapel`),
  ADD KEY `kd_jurusan` (`kd_jurusan`);

--
-- Indexes for table `siakad_jadwal`
--
ALTER TABLE `siakad_jadwal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kd_mapel` (`kd_mapel`),
  ADD KEY `kd_kelas` (`kd_kelas`),
  ADD KEY `nip` (`nip`);

--
-- Indexes for table `siakad_jurusan`
--
ALTER TABLE `siakad_jurusan`
  ADD PRIMARY KEY (`kd_jurusan`);

--
-- Indexes for table `siakad_kelas`
--
ALTER TABLE `siakad_kelas`
  ADD PRIMARY KEY (`kd_kelas`),
  ADD KEY `nip` (`nip`),
  ADD KEY `kd_jurusan` (`kd_jurusan`);

--
-- Indexes for table `siakad_mapel`
--
ALTER TABLE `siakad_mapel`
  ADD PRIMARY KEY (`kd_mapel`);

--
-- Indexes for table `siakad_nilai`
--
ALTER TABLE `siakad_nilai`
  ADD PRIMARY KEY (`nis`),
  ADD KEY `kd_kelas` (`kd_kelas`),
  ADD KEY `kd_mapel` (`kd_mapel`);

--
-- Indexes for table `siakad_pengguna`
--
ALTER TABLE `siakad_pengguna`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `siakad_rel_kelas`
--
ALTER TABLE `siakad_rel_kelas`
  ADD PRIMARY KEY (`nis`),
  ADD KEY `kd_kelas` (`kd_kelas`);

--
-- Indexes for table `siakad_siswa`
--
ALTER TABLE `siakad_siswa`
  ADD PRIMARY KEY (`nis`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
